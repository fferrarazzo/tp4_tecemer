// Ejemplo de un servicio REST accediendo a MySQL

var express=require('express');
var app = express();

var mysql      = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  port     : 3306,
  user     : 'root',
  password : 'Xj14p3r9',
  database : 'mydb'
});
 
connection.connect();


// Configuro respuesta para permitir accesos CORS
app.use(function(req,res,next) {
	res.header("Access-Control-Allow-Origin","*");
	res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
	next(); 
});

app.get('/supermercados',function(req,res) {
	console.log("GET /supermercados");
	connection.query('SELECT * from supermercados', function(err, rowsp, fields) {
		res.jsonp(rowsp)
	});
});

app.get('/supermercados/:id',function(req,res) {
        console.log("GET /supermercados/:id");
	var consulta = `SELECT * from supermercados WHERE id_supermercados = ${req.params.id}`;
        connection.query(consulta, function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/supermercados/:nombre/:ubicacion',function(req,res) {
        console.log("GET /supermercados/:nombre/:ubicacion");
        var consulta = 'INSERT INTO supermercados SET ?';
        connection.query(consulta, {nombre: req.params.nombre, ubicacion: req.params.ubicacion}, function(err, result) {
		if (err) throw err;
                res.jsonp(result.insertId)
        });
});

app.get('/productos',function(req,res) {
        console.log("GET /productos");
        connection.query('SELECT * from productos', function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/tipo',function(req,res) {
        console.log("GET /tipo");
        connection.query('SELECT * from tipo', function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.listen(8000);
console.log("REST POS instalado en port 8000");
